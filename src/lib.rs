/*
     rust-guile
    Copyright (C) 2021  Eric S. Londres

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, version 3 (only).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(improper_ctypes)]

use std::os;
use os::raw::{c_int, c_char, c_void};

include!("./bindings.rs");

extern "C" fn register_functions(_data: *mut c_void) -> *mut c_void {
    // right now this function does nothing
    // however, we need it for scm_with_guile()
    let ret: *mut c_void = std::ptr::null_mut();
    ret
}

pub fn init_scm() {
    // this function launches the scheme process and starts bootstrapping the runtime
    // any code executed after this function but before `scm_shell()' can talk to the scheme runtime
    unsafe {
	scm_with_guile(Some(register_functions), std::ptr::null_mut());
    }
}

pub fn run_scm(argc: c_int, argv: *mut *mut c_char) {
    // spawn the scheme shell, shifting execution from Rust mode to Guile mode
    unsafe {
	scm_shell(argc, argv);
    }
}

#[macro_export]
macro_rules! register_void_function {
    ($x:expr, $y:expr) => {
	// first convert the function name to a (const char *) so Guile can read it
	let ___fn_name: *const std::os::raw::c_char = std::ffi::CStr::from_bytes_with_nul($x).unwrap().as_ptr();
	// convert the function into a (void *) to match with Guile's execution expectations
	let ___function: *mut std::os::raw::c_void = $y as *mut std::os::raw::c_void;
	// register the function as a subroutine in Guile
	unsafe {
	    scm_c_define_gsubr(___fn_name, 0, 0, 0, ___function);
	}
    }
}
